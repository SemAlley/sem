'use strict';
var app = angular.module('sem', [
    'ngRoute',
    'ngResource'
]);
angular.module('sem')
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'indexCtrl',
                templateUrl: getPath('index')
            })
            .when('/blog', {
                controller: 'blogCtrl',
                templateUrl: getPath('blog/index')
            })
            .when('/post/:slug', {
                controller: 'postCtrl',
                templateUrl: getPath('blog/single')
            })
            .when('/trash', {
                controller: 'trashCtrl',
                templateUrl: getPath('trash')
            })
            .when('/register', {
                controller: 'registerCtrl',
                templateUrl: getPath('register/index')
            })
            .when('/profile', {
                controller: 'profileCtrl',
                templateUrl: getPath('profile/index')
            })
            .when('/profile/edit', {
                controller: 'profileEditCtrl',
                templateUrl: getPath('profile/edit')
            })
            .when('/admin', {
                controller: 'adminCtrl',
                templateUrl: getPath('admin/index')
            })
            .when('/admin/posts', {
                controller: 'adminPostsCtrl',
                templateUrl: getPath('admin/blog/index')
            })
            .when('/admin/post/create', {
                controller: 'adminPostCreateCtrl',
                templateUrl: getPath('admin/blog/create')
            })
            .when('/admin/post/update/:postId', {
                controller: 'adminPostUpdateCtrl',
                templateUrl: getPath('admin/blog/update')
            })
            .when('/admin/user', {
                controller: 'adminUserCtrl',
                templateUrl: getPath('admin/user/index')
            })
            .when('/admin/user/create', {
                controller: 'adminUserCreateCtrl',
                templateUrl: getPath('admin/user/create')
            })
            .when('/admin/user/update/:userId', {
                controller: 'adminUserUpdateCtrl',
                templateUrl: getPath('admin/user/update')
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);

function getPath(template) {
    var dirPath = 'view/';
    return dirPath + template + '.html';
}
angular.module('sem')
    .controller('blogCtrl', ['$scope', 'Article', function ($scope, Article) {
        $scope.articles = Article.query();
    }])
    .controller('postCtrl', ['$scope', function ($scope) {

    }]);

// angular.module('root', ['services', 'filters', 'ngResource'])
angular.module('sem')
    .config(["messageProvider", "messageText", function (messageProvider, messageText) {
        messageProvider.setText(messageText);
    }])
    .controller('indexCtrl', ['$scope', "message", function ($scope, message) {
        $scope.greating = message.text;
    }])
    .controller("trashCtrl", ["$scope", "userData", "roundFilter", "$resource", function ($scope, userData, round, $resource) {
        $scope.isBold = function () { return $scope.pisun % 2 === 0; };
        $scope.isItalic = function () { return $scope.pisun % 3 === 0; };
        $scope.isUnderlined = function () { return $scope.pisun % 5 === 0; };
        $scope.products = [
            {id: 1, name: "Hockey puck"},
            {id: 2, name: "Golf club"},
            {id: 3, name: "Baseball bat"},
            {id: 4, name: "Lacrosse stick"}
        ];
        $scope.isFirstElementVisible = true;
        $scope.isSecondElementVisible = true;
        $scope.currentUser = userData;

        $scope.factor = 6;
        $scope.$watch('factor', function (newValue, oldValue) {
            $scope.product = newValue * 2;
        });

        $scope.formSubmit = function (a, b) {
            console.log(a);
            console.log(b);
            console.log(1);
        };
        
        $scope.getUser = function () {
            var users = $resource("http://www.learn-angular.org/ResourceLesson/Users/:id");
            $scope.user = users.get({id: 1}, function (res) {
                console.log(res);
            });
        };

        $scope.postUser = function () {
            var response = $scope.user.$save(function () {
                alert("User saved!");
            });
        };

    }]);
angular.module('sem')
    .controller('profileCtrl', ['$scope', function ($scope) {
    
    }])
    .controller('profileEditCtrl', ['$scope', function ($scope) {
        
    }])
    .controller('registerCtrl', ['$scope', function ($scope) {
        
    }]);

angular.module('sem')
    .filter('round', function () {
        return function (input, precision) {
            return input ?
                parseFloat(input).toFixed(precision) :
                "";
        }
    })
    .filter('dollar', function () {
        return function (input) {
            return input ? '$' + input : "";
        }
    });
angular.module('sem')
    .controller('adminCtrl', ['$scope', function ($scope) {

    }]);
angular.module('sem')
    .controller('adminPostsCtrl', ['$scope', function ($scope) {

    }])
    .controller('adminPostCreateCtrl', ['$scope', 'Article', function ($scope, Article) {
        $scope.article = new Article();
        $scope.article.author = 'admin';
        $scope.formSubmit = function (form) {
            $scope.article.$save({test: '123'}).then(function (res) {
                form.$setPristine();
                console.log( res );
            }, function (err) {
                console.log( err );
            })
        }
    }])
    .controller('adminPostUpdateCtrl', ['$scope', function ($scope) {

    }]);
angular.module('sem')
    .controller('adminUserCtrl', ['$scope', function ($scope) {

    }])
    .controller('adminUserCreateCtrl', ['$scope', function ($scope) {

    }])
    .controller('adminUserUpdateCtrl', ['$scope', function ($scope) {

    }]);
angular.module('sem')
    .controller('navbarCtrl', ['$scope', '$location', function ($scope, $location) {
        $scope.menuItems = [
            {
                name: 'Blog',
                url: '#/blog'
            },
            {
                name: 'Trash',
                url: '#/trash'
            }
        ];
        $scope.routeIs = function(routeName) {
            return $location.path() === routeName.replace(/^#/, '');
        };
    }]);
var baseBlogUrl = 'http://localhost:1337/api/articles';

angular.module('sem')
    .factory('Article', ['$resource',
        function ($resource) {
            return $resource(baseBlogUrl + '/:postId',
                {'id': '@postId'},
                {
                    query: {method: 'GET', params: {id: ''}, isArray: true},
                    update: {method: 'PUT'},
                    create: {url: baseBlogUrl, method: "POST"}
                });
        }]);

angular.module('sem')
    .value("factor", 6)
    .constant("messageText", "Hello constant!")
    .factory('userData', [function () {
        return {
            id: 13,
            name: 'John Snow',
            role: 'walker'
        };
    }])
    .service('multiplier', ['factor', function (valueFactor) {
        this.multiply = function (controllerFactor) {
            return valueFactor * controllerFactor;
        };
    }])
    .provider("message", [function () {
        var text = null;
        this.setText = function (textString) {
            text = textString;
        };

        this.$get = [function () {
            return new Message(text);
        }];
    }]);

function Message(text) {
    this.text = text;
}