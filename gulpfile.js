'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('js', function () {
    gulp.src(['src/app.js', 'src/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./js'))
});

gulp.task('default', ['sass', 'js'], function () {
    gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch('./src/**/*js', ['js']);
});