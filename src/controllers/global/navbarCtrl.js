angular.module('sem')
    .controller('navbarCtrl', ['$scope', '$location', function ($scope, $location) {
        $scope.menuItems = [
            {
                name: 'Blog',
                url: '#/blog'
            },
            {
                name: 'Trash',
                url: '#/trash'
            }
        ];
        $scope.routeIs = function(routeName) {
            return $location.path() === routeName.replace(/^#/, '');
        };
    }]);