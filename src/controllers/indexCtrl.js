
// angular.module('root', ['services', 'filters', 'ngResource'])
angular.module('sem')
    .config(["messageProvider", "messageText", function (messageProvider, messageText) {
        messageProvider.setText(messageText);
    }])
    .controller('indexCtrl', ['$scope', "message", function ($scope, message) {
        $scope.greating = message.text;
    }])
    .controller("trashCtrl", ["$scope", "userData", "roundFilter", "$resource", function ($scope, userData, round, $resource) {
        $scope.isBold = function () { return $scope.pisun % 2 === 0; };
        $scope.isItalic = function () { return $scope.pisun % 3 === 0; };
        $scope.isUnderlined = function () { return $scope.pisun % 5 === 0; };
        $scope.products = [
            {id: 1, name: "Hockey puck"},
            {id: 2, name: "Golf club"},
            {id: 3, name: "Baseball bat"},
            {id: 4, name: "Lacrosse stick"}
        ];
        $scope.isFirstElementVisible = true;
        $scope.isSecondElementVisible = true;
        $scope.currentUser = userData;

        $scope.factor = 6;
        $scope.$watch('factor', function (newValue, oldValue) {
            $scope.product = newValue * 2;
        });

        $scope.formSubmit = function (a, b) {
            console.log(a);
            console.log(b);
            console.log(1);
        };
        
        $scope.getUser = function () {
            var users = $resource("http://www.learn-angular.org/ResourceLesson/Users/:id");
            $scope.user = users.get({id: 1}, function (res) {
                console.log(res);
            });
        };

        $scope.postUser = function () {
            var response = $scope.user.$save(function () {
                alert("User saved!");
            });
        };

    }]);