angular.module('sem')
    .controller('adminPostsCtrl', ['$scope', function ($scope) {

    }])
    .controller('adminPostCreateCtrl', ['$scope', 'Article', function ($scope, Article) {
        $scope.article = new Article();
        $scope.article.author = 'admin';
        $scope.formSubmit = function (form) {
            $scope.article.$save({test: '123'}).then(function (res) {
                form.$setPristine();
                console.log( res );
            }, function (err) {
                console.log( err );
            })
        }
    }])
    .controller('adminPostUpdateCtrl', ['$scope', function ($scope) {

    }]);