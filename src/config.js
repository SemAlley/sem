angular.module('sem')
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'indexCtrl',
                templateUrl: getPath('index')
            })
            .when('/blog', {
                controller: 'blogCtrl',
                templateUrl: getPath('blog/index')
            })
            .when('/post/:slug', {
                controller: 'postCtrl',
                templateUrl: getPath('blog/single')
            })
            .when('/trash', {
                controller: 'trashCtrl',
                templateUrl: getPath('trash')
            })
            .when('/register', {
                controller: 'registerCtrl',
                templateUrl: getPath('register/index')
            })
            .when('/profile', {
                controller: 'profileCtrl',
                templateUrl: getPath('profile/index')
            })
            .when('/profile/edit', {
                controller: 'profileEditCtrl',
                templateUrl: getPath('profile/edit')
            })
            .when('/admin', {
                controller: 'adminCtrl',
                templateUrl: getPath('admin/index')
            })
            .when('/admin/posts', {
                controller: 'adminPostsCtrl',
                templateUrl: getPath('admin/blog/index')
            })
            .when('/admin/post/create', {
                controller: 'adminPostCreateCtrl',
                templateUrl: getPath('admin/blog/create')
            })
            .when('/admin/post/update/:postId', {
                controller: 'adminPostUpdateCtrl',
                templateUrl: getPath('admin/blog/update')
            })
            .when('/admin/user', {
                controller: 'adminUserCtrl',
                templateUrl: getPath('admin/user/index')
            })
            .when('/admin/user/create', {
                controller: 'adminUserCreateCtrl',
                templateUrl: getPath('admin/user/create')
            })
            .when('/admin/user/update/:userId', {
                controller: 'adminUserUpdateCtrl',
                templateUrl: getPath('admin/user/update')
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);

function getPath(template) {
    var dirPath = 'view/';
    return dirPath + template + '.html';
}