var baseBlogUrl = 'http://localhost:1337/api/articles';

angular.module('sem')
    .factory('Article', ['$resource',
        function ($resource) {
            return $resource(baseBlogUrl + '/:postId',
                {'id': '@postId'},
                {
                    query: {method: 'GET', params: {id: ''}, isArray: true},
                    update: {method: 'PUT'},
                    create: {url: baseBlogUrl, method: "POST"}
                });
        }]);
