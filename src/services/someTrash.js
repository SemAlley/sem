angular.module('sem')
    .value("factor", 6)
    .constant("messageText", "Hello constant!")
    .factory('userData', [function () {
        return {
            id: 13,
            name: 'John Snow',
            role: 'walker'
        };
    }])
    .service('multiplier', ['factor', function (valueFactor) {
        this.multiply = function (controllerFactor) {
            return valueFactor * controllerFactor;
        };
    }])
    .provider("message", [function () {
        var text = null;
        this.setText = function (textString) {
            text = textString;
        };

        this.$get = [function () {
            return new Message(text);
        }];
    }]);

function Message(text) {
    this.text = text;
}