
angular.module('sem')
    .filter('round', function () {
        return function (input, precision) {
            return input ?
                parseFloat(input).toFixed(precision) :
                "";
        }
    })
    .filter('dollar', function () {
        return function (input) {
            return input ? '$' + input : "";
        }
    });